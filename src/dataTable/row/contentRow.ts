import { shuffle } from '../../util';
import { Cell } from '../cell/cell';
import { ContentCell } from '../cell/contentCell';
import { DataTable } from '../dataTable';
import { Row } from './row';

export class ContentRow extends Row {
  constructor(parent: DataTable, cells: Cell[] = []) {
    super(parent, cells);
  }

  public delete() {
    this.table.deleteRow(this);
  }

  public addCell(cellContents: any, cellClasses?: string[]) {
    super.addFormedCell(new ContentCell(this, cellContents, cellClasses));
  }

  public addCellFromJSON(json: string) {
    super.addFormedCell(ContentCell.fromJSON(this, json));
  }

  public getCell(cellIDX: number): ContentCell {
    if (!(cellIDX > -1 && cellIDX < this.cells.length)) { return undefined; }
    return this.cells[cellIDX] as ContentCell;
  }

  public redactRandomCells(numCells: number) {
    const toRedact: Cell[] = shuffle(this.cells.filter((c) => c.isRedactable)).filter( (c, i) => i < numCells);
    toRedact.forEach((c) => {
      c.isRedacted = true;
    });
  }

}
