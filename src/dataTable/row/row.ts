import { Cell } from '../cell/cell';
import { DataTable } from '../dataTable';
import { IRowArgs } from './rowArgs';

export class Row {
  public static fromArray<T extends typeof Row>(this: T, parent: DataTable, cellArray: any[]): InstanceType<T> {
    const row = new this(parent);
    cellArray.forEach((c) => {
      row.addCell(c);
    });
    return row as InstanceType<T>;
  }

  public static fromJSON<T extends typeof Row>(this: T, parent: DataTable, jsonObj: any): InstanceType<T> {
    const row = new this(parent);
    jsonObj.cells.forEach((c: any) => row.addCellFromJSON(c));
    row.title = jsonObj.title;
    row.cssClasses = jsonObj.cssClasses;
    return row as InstanceType<T>;
  }

  public static emptyRowLike<T extends typeof Row>(this: T, parent: DataTable, likeArray: any[]): InstanceType<T> {
    const row = new this(parent);
    likeArray.forEach((c) => row.addCell(''));
    return row as InstanceType<T>;
  }

  public title: string;
  public cssClasses: string[];
  public cells: Cell[] = [];
  public get numCells(): number { return this.cells.length; }
  protected domElement: HTMLTableRowElement;
  protected table: DataTable;

  public get length(): number {
    return this.cells.length;
  }

  constructor(parent: DataTable, cells: Cell[] = []) {
    this.table = parent;
    this.cells = cells;
  }

  public toDomElement(args: IRowArgs): HTMLTableRowElement {
    this.domElement = document.createElement('tr');
    this.updateDomElement();

    return this.domElement;
  }

  public get jsonObject(): object {
    return {
      cells: this.cells.map((c) => c.jsonObject),
      cssClasses: this.cssClasses,
      title: this.title,
    };
  }

  public toJSON(replacer: (key: string, value: any) => any, pretty?: string|number) {
    return JSON.stringify(this.jsonObject, replacer, pretty);
  }

  public getCellIDX<T extends Cell>(cell: T): number {
    return this.cells.indexOf(cell);
  }

  public deleteColumn(cell: Cell) {
    const colIDX = this.cells.indexOf(cell);
    if (colIDX < 0 ) { return; }
    this.table.deleteColumn(colIDX);
  }

  public removeCell(cellIDX: number, updateDomElement: boolean = false) {
    if (!(cellIDX > -1 && cellIDX < this.cells.length)) { return; }
    this.cells.splice(cellIDX, 1);
    if (updateDomElement) { this.updateDomElement(); }
  }

  protected addCell(contents: any, cellClasses: string[] = []) {
    this.cells.push(new Cell(contents, cellClasses));
    this.updateDomElement();
  }

  protected addCellFromJSON(json: string) {
    throw new Error(`This method is a placeholder method \
    for derived classes and should not be \
    used directly`);
  }

  protected addFormedCell<T extends Cell>(cell: T) {
    this.cells.push(cell);
  }

  protected updateDomElement() {
    if (!this.domElement) {
      return;
    }

    this.removeDomCells();

    for (const c of this.cells) {
      this.domElement.appendChild(c.toDomElement({}));
    }
  }

  private removeDomCells() {
    if (!this.domElement) { return; }
    while (this.domElement.hasChildNodes()) {
      this.domElement.removeChild(this.domElement.lastChild);
    }
  }

  private getCellChildren(): Element[] {
    const cellChildren = [];
    const domChildren = this.domElement.children;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < domChildren.length ; i++) {
      if (domChildren[i].id === 'AddABlankColumn') {
        continue;
      }
      cellChildren.push(domChildren[i]);
    }
    return cellChildren;
  }
}
