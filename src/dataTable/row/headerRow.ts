import { Cell } from '../cell/cell';
import { ContentCell } from '../cell/contentCell';
import { HeaderCell } from '../cell/headerCell';
import { DataTable } from '../dataTable';
import { Row } from './row';
import { IRowArgs } from './rowArgs';

export class HeaderRow extends Row {
  public static getFooter(parent: DataTable): HTMLTableRowElement {
    const row = new Row(parent).toDomElement({});
    row.appendChild(Cell.addRemoveRowFromDataTableCell(parent).toDomElement({
      editable: true,
    }));
    return row;
  }

  public addCell(cellContents: any, cellClasses?: string[]) {
    super.addFormedCell(new HeaderCell(this, cellContents, cellClasses));
  }

  public addCellFromJSON(json: string) {
    super.addFormedCell(HeaderCell.fromJSON(this, json));
  }

  public getColumn(cellIDX: number): ContentCell[] {
    if (!(cellIDX > -1 && cellIDX < this.cells.length)) { return undefined; }
    return this.table.rows.map((r) => r.getCell(cellIDX));
  }

  public toDomElement(args: IRowArgs): HTMLTableRowElement {
    super.toDomElement(args);
    if (args.includeAddColumnButton) {
      this.addAppendButton();
    }

    return this.domElement;
  }

  private addAppendButton() {
    const addColumnCell = Cell.addRemoveColumnFromDataTableCell(this.table).toDomElement({
      editable: false,
      id: 'AddABlankColumn',
    });
    this.domElement.appendChild(addColumnCell);
  }

  private removeAppendButton() {
    if (!this.domElement) { return; }
    const appendChild = this.getAppendColumnChildElement();
    if (!appendChild) { return; }
    this.domElement.removeChild(this.getAppendColumnChildElement());
  }

  private getAppendColumnChildElement(): Element {
    const domChildren = this.domElement.children;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < domChildren.length ; i ++) {
      if (domChildren[i].id === 'AddABlankColumn') {
        return domChildren[i];
      }
    }
    return undefined;
  }
}
