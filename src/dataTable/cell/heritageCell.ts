import { Row } from '../row/row';
import { Cell } from './cell';

export class HeritageCell extends Cell {
  public parent: Row;

  constructor(parent: Row, contents: any, classes: string[] = []) {
    super(contents, classes);
    this.parent = parent;
  }

  public deleteParentColumn() {
    this.parent.deleteColumn(this);
  }
}
