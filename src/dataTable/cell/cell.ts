import { removeDomChildren } from '../../util';
import { DataTable } from '../dataTable';
import { ICellArgs } from './cellArgs';
import { CellStyles } from './cellStyles';

enum cellType {
  header,
  content,
}

export class Cell {
  public static addRemoveColumnFromDataTableCell(dataTable: DataTable): Cell {
    const { cell, addLi, subtractLi } = this.addRemoveCell();

    addLi.addEventListener('click', () => dataTable.addColumn());
    subtractLi.addEventListener('click', () => dataTable.deleteColumn(dataTable.headerRow.length - 1));

    return cell;
  }

  public static addRemoveRowFromDataTableCell(dataTable: DataTable): Cell {
    const { cell, addLi, subtractLi } = this.addRemoveCell();

    addLi.addEventListener('click', () => dataTable.addRow());
    subtractLi.addEventListener('click', () => dataTable.deleteRow(dataTable.rows[dataTable.rows.length - 1]));

    return cell;
  }

  private static addRemoveCell() {
    const list = document.createElement('ul');
    list.classList.add('tightul');
    const addLi = document.createElement('li');
    const subtractLi = document.createElement('li');
    list.appendChild(addLi);
    list.appendChild(subtractLi);

    const addImg = document.createElement('img');
    addImg.src = '../views/add.svg';
    addLi.appendChild(addImg);

    const subtractImg = document.createElement('img');
    subtractImg.src = '../views/subtract.svg';
    subtractLi.appendChild(subtractImg);

    addImg.classList.add('addToTable');
    subtractImg.classList.add('addToTable');

    return { cell: new Cell(list), addLi, subtractLi };
  }

  public contents: any;
  public cssClasses: string[];
  protected contextMenuCallback: () => void;
  private internalStyle: CellStyles = CellStyles.Normal;
  private redactable: boolean;
  private editable: boolean;
  private tdElement: HTMLTableCellElement;
  private spanElement: HTMLSpanElement;
  private lastArgs: ICellArgs;
  private handleRedactedMouseUp: () => void;
  private handleRedactedMouseDown: () => void;

  constructor(contents: any, classes: string[] = []) {
    this.contents = contents;
    this.cssClasses = classes;
    this.redactable = true;
  }

  get cellStyle(): CellStyles {
    return this.internalStyle;
  }

  set cellStyle(newStyle: CellStyles) {
    const styleIDX = this.cssClasses.indexOf(this.internalStyle);
    if (styleIDX > -1) {
      this.cssClasses.splice(styleIDX, 1);
    }
    this.internalStyle = newStyle;
    this.cssClasses.push(newStyle);
    this.updateDomElement();
  }

  get isRedacted(): boolean {
    return this.cssClasses.indexOf('redacted') > -1;
  }

  set isRedacted(newState: boolean) {
    const alreadyRedacted = this.isRedacted;
    if (newState && !alreadyRedacted && this.isRedactable) {
      this.cssClasses.push('redacted');
    } else if (!newState && alreadyRedacted) {
      let redactedIDX = this.cssClasses.indexOf('redacted');
      while (redactedIDX > -1) {
        this.cssClasses.splice(redactedIDX, 1);
        redactedIDX = this.cssClasses.indexOf('redacted');
      }
    } else {
      return;
    }
    this.domRedacted(newState);
  }

  get isRedactable(): boolean {
    return this.redactable;
  }

  set isRedactable(newState: boolean) {
    this.redactable = newState;
    if (newState) {
      this.isEditable = false;
    } else {
      this.isRedacted = false;
    }
  }

  get isEditable(): boolean {
    return this.editable;
  }
  set isEditable(newState: boolean) {
    this.editable = newState;
    if (newState) { this.isRedactable = false; }
    this.domEditable(newState);
  }

  public toDomElement(args: ICellArgs): HTMLTableDataCellElement {
    this.tdElement = document.createElement('td');
    this.spanElement = document.createElement('span');
    this.lastArgs = args;

    this.tdElement.addEventListener('input', () => {
      this.contents = this.tdElement.innerText;
    });

    this.setContextMenu();

    this.updateDomElement();

    return this.tdElement;
  }

  public get jsonObject(): object {
    return {
      contents : this.contents,
      cssClasses: this.cssClasses,
      editable: this.editable,
      internalStyle: this.internalStyle,
      lastArgs: this.lastArgs,
      redactable: this.redactable,
      redacted: this.isRedacted,
    };
  }

  public toJSON(replacer?: (key: string, value: any) => any,  pretty?: string|number) {
    return JSON.stringify(this.jsonObject, replacer, pretty);
  }

  private updateDomElement(args?: ICellArgs) {
    if (!(args || this.lastArgs)) { return; }
    if (!(this.tdElement && this.spanElement)) { return; }
    if (args) { this.lastArgs = args; }

    removeDomChildren(this.tdElement);
    this.tdElement.className = '';

    if (this.contents instanceof HTMLElement) {
      this.spanElement.appendChild(this.contents);
    } else {
      this.spanElement.innerText = this.contents;
    }
    this.tdElement.appendChild(this.spanElement);

    if (this.lastArgs.clickCallback) {
      ((this.lastArgs.minimizeClickArea) ? this.spanElement : this.tdElement)
        .addEventListener('click', this.lastArgs.clickCallback);
    }

    if (this.lastArgs.id) {
      this.tdElement.id = this.lastArgs.id;
    }

    this.tdElement.classList.add(...this.cssClasses);

    this.domEditable(this.lastArgs.editable);

    this.domRedacted(this.isRedacted);
  }

  private setContextMenu() {
    if (this.lastArgs.contextMenu) {
      this.contextMenuCallback = this.lastArgs.contextMenu;
    }
    this.tdElement.addEventListener('contextmenu', this.contextMenuCallback);
  }

  private domRedacted(isRedacted: boolean) {
    if (this.tdElement) {
      if (!this.handleRedactedMouseDown) {
        this.handleRedactedMouseDown = () => { this.tdElement.classList.remove('redacted'); };
      }
      if (!this.handleRedactedMouseUp) {
        this.handleRedactedMouseUp = () => { this.tdElement.classList.add('redacted'); };
      }
      if (isRedacted) {
        this.isEditable = false;
        this.tdElement.classList.add('redacted');
        this.tdElement.addEventListener('mousedown', this.handleRedactedMouseDown);
        this.tdElement.addEventListener('mouseup', this.handleRedactedMouseUp);
      } else {
        this.tdElement.classList.remove('redacted');
        this.tdElement.removeEventListener('mousedown', this.handleRedactedMouseDown);
        this.tdElement.removeEventListener('mouseup', this.handleRedactedMouseUp);
      }
    }
  }

  private domEditable(isEditable: boolean) {
    if (this.tdElement) {
      this.tdElement.contentEditable = `${isEditable === true}`;
    }
  }
}
