import { popupHeaderCellContextMenu } from '../menu/headerCellContextMenu';
import { HeaderRow } from '../row/headerRow';
import { HeritageCell } from './heritageCell';

export class HeaderCell extends HeritageCell {
  public static fromJSON(parent: HeaderRow, jsonObj: any): HeaderCell {
    const cell = new HeaderCell(parent, jsonObj.contents);
    cell.cssClasses = jsonObj.cssClasses;
    cell.isEditable = jsonObj.editable;
    cell.cellStyle = jsonObj.internalStyle;
    cell.isRedactable = jsonObj.redactable;
    cell.isRedacted = jsonObj.redacted;
    // tslint:disable-next-line:no-string-literal
    cell['lastArgs'] = jsonObj.lastArgs;
    return cell;
  }

  public parent: HeaderRow;
  private columnRedactable: boolean = true;

  constructor(parent: HeaderRow, contents: any, classes: string[] = []) {
    super(parent, contents, classes);
    this.contextMenuCallback = () => popupHeaderCellContextMenu(this);
  }

  get columnIsRedactable(): boolean {
    return this.columnRedactable;
  }

  set columnIsRedactable(newState: boolean) {
    this.columnRedactable = newState;
    this.parent.getColumn(this.parent.getCellIDX(this)).forEach((c) => {
      c.isRedactable = newState;
    });
  }

  public deleteParentColumn() {
    this.parent.deleteColumn(this);
  }
}
