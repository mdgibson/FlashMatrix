import { popupContentCellContextMenu } from '../menu/contentCellContextMenu';
import { ContentRow } from '../row/contentRow';
import { HeritageCell } from './heritageCell';

export class ContentCell extends HeritageCell {
  public static fromJSON(parent: ContentRow, jsonObj: any): ContentCell {
    const cell = new ContentCell(parent, jsonObj.contents);
    cell.cssClasses = jsonObj.cssClasses;
    cell.isEditable = jsonObj.editable;
    cell.cellStyle = jsonObj.internalStyle;
    cell.isRedactable = jsonObj.redactable;
    cell.isRedacted = jsonObj.redacted;
    // tslint:disable-next-line:no-string-literal
    cell['lastArgs'] = jsonObj.lastArgs;
    return cell;
  }

  public parent: ContentRow;

  constructor(parent: ContentRow, contents: any, classes: string[] = []) {
    super(parent, contents, classes);
    this.contextMenuCallback = () => popupContentCellContextMenu(this);
  }


  public deleteParentRow() {
    this.parent.delete();
  }
}
