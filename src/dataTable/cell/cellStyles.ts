export enum CellStyles {
  Normal = 'normal',
  Good = 'good',
  Intermediate = 'intermediate',
  Bad = 'bad',
}
