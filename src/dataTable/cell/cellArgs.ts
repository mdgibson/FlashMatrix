export interface ICellArgs {
  clickCallback?: () => any;
  id?: string;
  minimizeClickArea?: boolean;
  editable?: boolean;
  contextMenu?: () => void;
}
