import { MenuItemConstructorOptions, remote } from 'electron';
import { CellStyles } from '../cell/cellStyles';
import { ContentCell } from '../cell/contentCell';

function generateContentCellMenu(cell: ContentCell) {
  const { Menu } = remote;
  const dataCellMenuTemplate: MenuItemConstructorOptions[] = [
    {
      checked: cell.isRedacted,
      click: (item) => { cell.isRedacted = item.checked; },
      label: 'Redacted',
      type: 'checkbox',
    },
    {
      checked: cell.isEditable,
      click: (item) => { cell.isEditable = item.checked; },
      label: 'Editable',
      type: 'checkbox',
    },
    {
      checked: cell.isRedactable,
      click: (item) => { cell.isRedactable = item.checked; },
      label: 'Redactable',
      type: 'checkbox',
    },
    {
      label: 'Style',
      submenu: [
        {
          checked: cell.cssClasses.indexOf(CellStyles.Normal) > -1,
          click: (item) => { cell.cellStyle = CellStyles.Normal; },
          label: 'None',
          type: 'radio',
        },
        {
          checked: cell.cssClasses.indexOf(CellStyles.Good) > -1,
          click: (item) => { cell.cellStyle = CellStyles.Good; },
          label: 'Good',
          type: 'radio',
        },
        {
          checked: cell.cssClasses.indexOf(CellStyles.Intermediate) > -1,
          click: (item) => { cell.cellStyle = CellStyles.Intermediate; },
          label: 'Needs Work',
          type: 'radio',
        },
        {
          checked: cell.cssClasses.indexOf(CellStyles.Bad) > -1,
          click: (item) => { cell.cellStyle = CellStyles.Bad; },
          label: 'Bad',
          type: 'radio',
        },
      ],
    },
    {
      type: 'separator',
    },
    {
      label: 'Delete Row',
      click: () => cell.deleteParentRow(),
    },
    {
      label: 'Delete Column',
      click: () => cell.deleteParentColumn(),
    },
  ];

  return Menu.buildFromTemplate(dataCellMenuTemplate);
}

export function popupContentCellContextMenu(cell: ContentCell) {
  const dataCellMenu = generateContentCellMenu(cell);
  dataCellMenu.popup({window: remote.getCurrentWindow()});
}
