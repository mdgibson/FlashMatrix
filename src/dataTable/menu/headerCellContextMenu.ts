import { MenuItemConstructorOptions, remote } from 'electron';
import { HeaderCell } from '../cell/headerCell';

function generateHeaderCellMenu(cell: HeaderCell) {
  const { Menu } = remote;
  const dataCellMenuTemplate: MenuItemConstructorOptions[] = [
    // {
    //   label: 'Column Style',
    //   submenu: [
    //     {
    //       checked: cell.cssClasses.indexOf(cellStyles.Normal) > -1,
    //       click: (item) => { cell.cellStyle = cellStyles.Normal; },
    //       label: 'None',
    //       type: 'radio',
    //     },
    //     {
    //       checked: cell.cssClasses.indexOf(cellStyles.Good) > -1,
    //       click: (item) => { cell.cellStyle = cellStyles.Good; },
    //       label: 'Good',
    //       type: 'radio',
    //     },
    //     {
    //       checked: cell.cssClasses.indexOf(cellStyles.Intermediate) > -1,
    //       click: (item) => { cell.cellStyle = cellStyles.Intermediate; },
    //       label: 'Needs Work',
    //       type: 'radio',
    //     },
    //     {
    //       checked: cell.cssClasses.indexOf(cellStyles.Bad) > -1,
    //       click: (item) => { cell.cellStyle = cellStyles.Bad; },
    //       label: 'Bad',
    //       type: 'radio',
    //     },
    //   ],
    // },
    {
      checked: cell.columnIsRedactable,
      click: (item) => { cell.columnIsRedactable = item.checked; },
      label: 'Column redactable',
      type: 'checkbox',
    },
    {
      click: () => cell.deleteParentColumn(),
      label: 'Delete Column',
    },
    {
      checked: cell.isEditable,
      click: (item) => { cell.isEditable = item.checked; },
      label: 'Cell is Editable',
      type: 'checkbox',
    },
  ];

  return Menu.buildFromTemplate(dataCellMenuTemplate);
}

export function popupHeaderCellContextMenu(cell: HeaderCell) {
  const dataCellMenu = generateHeaderCellMenu(cell);
  dataCellMenu.popup({window: remote.getCurrentWindow()});
}
