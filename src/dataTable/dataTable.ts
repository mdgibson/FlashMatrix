import { csvParse, csvParseRows } from 'd3-dsv';
import { removeDomChildren, shuffle } from '../util';
import { Cell } from './cell/cell';
import { ContentRow } from './row/contentRow';
import { HeaderRow } from './row/headerRow';

export class DataTable {
  public static fromStringArray({
    hasHeader = true,
    rowArray,
    filePath,
  }: {
    hasHeader?: boolean,
    rowArray: any[][],
    filePath?: string,
  }): DataTable {
    const csv = rowArray.map((r) => {
      return `"${r.join('","')}"`;
    }).join('\n');

    return this.fromCsv({
      csvString: csv,
      hasHeader,
      filePath,
    });
  }

  public static fromJSON(filePath: string, json: string): DataTable {
    const jsonObj = JSON.parse(json);
    const dataTable = new DataTable(filePath);
    dataTable.headerRow = (jsonObj.headerRow) ? HeaderRow.fromJSON(dataTable, jsonObj.headerRow) : undefined;
    dataTable.rows = jsonObj.rows.map((r: string) => ContentRow.fromJSON(dataTable, r));
    return dataTable;
  }

  public static fromCsv({
    filePath,
    csvString,
    hasHeader = true,
  }: {
    filePath?: string,
    csvString: string,
    hasHeader?: boolean,
  }): DataTable {
    if (hasHeader) {
      return this.fromCsvWithHeader(csvString, filePath);
    } else {
      return this.fromCsVWithoutHeader(csvString, filePath);
    }
  }

  private static fromCsvWithHeader(csvString: string, filePath?: string): DataTable {
    const dataTable = new DataTable(filePath);
    const parsed = csvParse(csvString);

    dataTable.headerRow = HeaderRow.fromArray(dataTable, parsed.columns);
    parsed.forEach((r) => {
      const row = parsed.columns.map((h) => r[h]);
      dataTable.addRow(ContentRow.fromArray(dataTable, row));
    });
    return dataTable;
  }

  private static fromCsVWithoutHeader(csvString: string, filePath?: string): DataTable {
    const dataTable = new DataTable(filePath);
    const parsed = csvParseRows(csvString);

    if (parsed.length === 0) {
      return dataTable;
    }

    dataTable.headerRow = HeaderRow.emptyRowLike(dataTable, parsed[0]);
    parsed.forEach((r) => {
      dataTable.addRow(ContentRow.fromArray(dataTable, r));
    });
    return dataTable;
  }

  public filePath: string;
  public headerRow: HeaderRow = new HeaderRow(this);
  public rows: ContentRow[] = [];
  private domElement: HTMLTableElement;

  constructor(filePath?: string) {
    this.filePath = filePath;
  }

  public toDomElement(): HTMLTableElement {
    this.domElement = document.createElement('table');
    this.domElement.classList.add('table', 'table-hover', 'table-striped', 'table-bordered', 'w-auto');

    this.updateDomElement();

    return this.domElement;
  }

  public toJSON(replacer?: (key: string, value: any) => any, pretty?: string|number): string {
    return JSON.stringify({
      headerRow: this.headerRow.jsonObject,
      rows: this.rows.map((r) => r.jsonObject),
    }, replacer, pretty);
  }

  public updateDomElement() {
    if (!this.domElement) {
      return;
    }

    removeDomChildren(this.domElement);

    const thead = document.createElement('thead');
    const tbody = document.createElement('tbody');
    const tfoot = document.createElement('tfoot');
    this.domElement.appendChild(thead);
    this.domElement.appendChild(tbody);
    this.domElement.appendChild(tfoot);

    thead.appendChild(this.headerRow.toDomElement({includeAddColumnButton: true}));

    this.rows.forEach((row) => {
      tbody.appendChild(row.toDomElement({}));
    });

    tfoot.appendChild(HeaderRow.getFooter(this));
  }

  public addRow(row ?: ContentRow) {
    if (!row) {
      row = ContentRow.emptyRowLike(this, this.headerRow.cells);
    }
    row = this.validateRow(row);
    this.rows.push(row);
    this.updateDomElement();
  }

  public deleteRow(row: ContentRow) {
    const rowIDX = this.rows.indexOf(row);
    if (rowIDX < 0) { return; }

    this.rows.splice(rowIDX, 1);
    this.updateDomElement();
  }

  public addColumn() {
    this.headerRow.addCell('');
    this.rows.forEach((r) => { r.addCell(''); });
    this.updateDomElement();
  }

  public deleteColumn(colIDX: number) {
    this.headerRow.removeCell(colIDX);
    this.rows.forEach((r) => r.removeCell(colIDX));
    this.updateDomElement();
  }

  public addRows(rows: ContentRow[]) {
    rows.forEach((r) => {
      r = this.validateRow(r);
      this.rows.push(r);
    });
    this.updateDomElement();
  }

  public disableEdit() {
    this.applyToAllCells((c) => {
      c.isEditable = false;
    });
  }

  public redactRandomCells(difficultyInput: HTMLInputElement) {
    const redactionsPerRow: number = Number(difficultyInput.value);
    this.rows.forEach((r) => {
      r.redactRandomCells(redactionsPerRow);
    });
  }

  public revealAllCells() {
    this.applyToAllCells((c) => {
      c.isRedacted = false;
    });
  }

  public shuffleRows() {
    this.rows = shuffle(this.rows);
    this.updateDomElement();
  }

  private applyToAllCells(func: (cell: Cell) => void) {
    this.rows.forEach((r) => {
      r.cells.forEach((c) => {
        func(c);
      });
    });
  }

  private validateRow(row: ContentRow): ContentRow {
    const diff = this.headerRow.numCells - row.numCells;
    if (diff > 0) {
      for (let i = 0; i < diff ; i++) {
        row.addCell('');
      }
    } else if (diff < 0) {
      for (let i = 0; i > diff ; i--) {
        this.addColumn();
      }
    }

    return row;
  }
}
