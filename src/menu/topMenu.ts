import { MenuItemConstructorOptions, remote } from 'electron';
import fs = require('fs');
import path = require('path');
import { DataTable } from '../dataTable/dataTable';
import { removeDomChildren } from '../util';
import { setNavButtons } from './nav';
const { Menu, dialog } = remote;

let lastFileName: string;
let requestOverwrite = false;

function generateTopMenu(dataTable: DataTable): Electron.Menu {
  const template: MenuItemConstructorOptions[] = [
    {
      label: 'File',
      submenu: [
        {
          accelerator: 'ctrl + o',
          click: () => openFile(dataTable),
          label: 'open',
        },
        {
          accelerator: 'ctrl + s',
          click: () => saveFile(dataTable),
          label: 'save',
        },
        {
          accelerator: 'ctrl + shift + s',
          click: () => saveFileAs(dataTable),
          label: 'save as...',
        },
        {
          role: 'quit',
        },
      ],
    },
    {
      label: 'Edit',
      submenu: [
        {
          role: 'undo',
        },
        {
          role: 'redo',
        },
        {
          type: 'separator',
        },
        {
          role: 'cut',
        },
        {
          role: 'copy',
        },
        {
          role: 'paste',
        },
        {
          role: 'pasteandmatchstyle',
        },
        {
          role: 'delete',
        },
        {
          role: 'selectall',
        },
      ],
    },
    {
      label: 'View',
      submenu: [
        {
          accelerator: 'CmdOrCtrl+R',
          label: 'Reload',
          click(item, focusedWindow) {
            if (focusedWindow) { focusedWindow.reload(); }
          },
        },
        {
          accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
          label: 'Toggle Developer Tools',
          click(item, focusedWindow) {
            if (focusedWindow) { focusedWindow.webContents.toggleDevTools(); }
          },
        },
        {
          type: 'separator',
        },
        {
          role: 'resetzoom',
        },
        {
          role: 'zoomin',
        },
        {
          role: 'zoomout',
        },
        {
          type: 'separator',
        },
        {
          role: 'togglefullscreen',
        },
      ],
    },
    {
      role: 'window',
      submenu: [
        {
          role: 'minimize',
        },
        {
          role: 'close',
        },
      ],
    },
  ];

  return Menu.buildFromTemplate(template);
}

// TODO: support mac menuing

function openFile(dataTable: DataTable) {
  dialog.showOpenDialog({
    filters: [{ name: 'FlashMatrix files', extensions: ['fm'] },
              { name: 'csv files', extensions: ['csv'] },
              { name: 'any file', extensions: ['*'] }],
    properties: ['openFile'],
  }, (fileNames: string[]) => {
    if (!fileNames) { return; }

    const fileName = fileNames[0];
    const extension = path.extname(fileName);
    fs.readFile(fileName, 'utf-8', (err, data) => {
      if (extension === '.csv') {
        requestOverwrite = true;
        dataTable = DataTable.fromCsv({
              csvString: data,
              filePath: fileName.replace(extension, '.fm'),
            });
      } else if (extension === '.fm') {
        dataTable = DataTable.fromJSON(fileName, data);
      } else { dialog.showErrorBox('Unknown FileType', `Error, unknown filetype: ${extension}`); }

      lastFileName = path.basename(fileName, extension);

      const el = document.getElementById('table');
      removeDomChildren(el);
      el.appendChild(dataTable.toDomElement());
      setNavButtons(dataTable);
      setTopMenu(dataTable);
      });
  });
}

function saveFile(dataTable: DataTable) {
  if (!dataTable.filePath) { return saveFileAs(dataTable); }

  if (requestOverwrite) {
    fs.access(dataTable.filePath, (err) => {
      if (!err) {
        dialog.showMessageBox({
          type: 'warning',
          buttons: ['OK', 'Cancel'],
          cancelId: 1,
          title: 'Overwrite file?',
          message: `The file\n\t${dataTable.filePath}\nalready exists. Is it OK to overwrite?`,
        }, (btnIDX) => {
          if (btnIDX === 0) {
            writeFile(dataTable, dataTable.filePath);
            requestOverwrite = false;
          }
        });
      }
    });
  } else {
    writeFile(dataTable, dataTable.filePath);
  }
}

function saveFileAs(dataTable: DataTable) {
  dialog.showSaveDialog({
    defaultPath: lastFileName,
    filters: [{ name: 'FlashMatrix files', extensions: ['fm'] },
                { name: 'any file', extensions: ['*'] }],
  }, (filePath: string) => {
    if (!filePath) { return; }
    dataTable.filePath = filePath;
    writeFile(dataTable, dataTable.filePath);
  });
}

function writeFile(dataTable: DataTable, filePath: string) {
  fs.writeFile(filePath, dataTable.toJSON(null, 2), (err) => {
    if (err) {
      dialog.showErrorBox(
        'Save error',
        `Error during saving, data not saved: ${err.message}`,
      );
    } else {
      lastFileName = path.basename(filePath, path.extname(filePath));
    }
  });
}

export function setTopMenu(dataTable: DataTable) {
  Menu.setApplicationMenu(generateTopMenu(dataTable));
}
