import { DataTable } from '../dataTable/dataTable';
import { removeDomChildren } from '../util';

export function setNavButtons(table: DataTable) {
  const redact = document.getElementById('redactRandom');
  removeDomChildren(redact);
  const redactSpan = document.createElement('span');
  redactSpan.classList.add('nav-link');
  redactSpan.innerText = 'Study!';
  redactSpan.addEventListener('click', () => {
    table.revealAllCells();
    table.disableEdit();
    table.redactRandomCells(document.getElementById('difficulty') as HTMLInputElement);
  });
  redact.appendChild(redactSpan);

  const reveal = document.getElementById('revealAll');
  removeDomChildren(reveal);
  const revealSpan = document.createElement('span');
  revealSpan.classList.add('nav-link');
  revealSpan.innerText = 'Reveal All';
  revealSpan.addEventListener('click', () => {
    table.revealAllCells();
  });
  reveal.appendChild(revealSpan);

  const shuffle = document.getElementById('shuffleRows');
  removeDomChildren(shuffle);
  const shuffleSpan = document.createElement('span');
  shuffleSpan.classList.add('nav-link');
  shuffleSpan.innerText = 'Shuffle Rows';
  shuffleSpan.addEventListener('click', () => {
    table.shuffleRows();
  });
  shuffle.appendChild(shuffleSpan);
}
