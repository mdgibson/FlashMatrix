import { DataTable } from './dataTable/dataTable';
import { setNavButtons } from './menu/nav';
import { setTopMenu } from './menu/topMenu';

// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const data = [
  ['', 'Ford', 'Tesla', 'Toyota', 'Honda'],
  ['2017', 10, 11, 12, 13],
  ['2018', 20, 11, 14, 13],
  ['2019', 30, 15, 12, 13],
];

const tableEl = document.getElementById('table');
const dataTable = DataTable.fromStringArray({
  rowArray: data,
});
dataTable.rows[0].cells[2].isRedacted = true;
tableEl.appendChild(dataTable.toDomElement());
setNavButtons(dataTable);
setTopMenu(dataTable);
