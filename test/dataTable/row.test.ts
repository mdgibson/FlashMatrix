import * as chai from 'chai';
import { DataTable } from '../../src/dataTable/dataTable';
import { Row } from '../../src/dataTable/row/row';

// tslint:disable-next-line:no-var-requires
require('jsdom-global')();
const should = chai.should();

describe('Row', () => {
  const dataTable = new DataTable();
  let row: Row;

  beforeEach(() => {
    row = new Row(dataTable);
  });

  it('should exist', () => {
    row.should.exist;
  });

  it('should maintain a reference to its parent table', () => {
    row.should.have.property('table')
      .that.is.an.instanceof(DataTable);
    // tslint:disable-next-line:no-string-literal
    row['table'].should.equal(dataTable);
  });

  it ('should be able to instantiate from an array');

  it ('should be able to instantiate from a JSON string');

  it ('should be able to create an empty row like a given array');

  it ('should store an array of Cells');

  it ('should have a property which gives the current cell count');

  it ('should maintain its domElement');

  it ('should be able to instantiate with an array of cells');

  it ('should create a complete object literal of itself');

  it ('should be able to instantiate itself from a JSON object created by it');

  it ('should have an addCellFromJSON method that throws an error');

  it ('should be able to add Cells');

  it ('should update its existing dom element');
});
