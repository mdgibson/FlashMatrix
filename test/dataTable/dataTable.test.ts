import * as chai from 'chai';
import { Cell } from '../../src/dataTable/cell/cell';
import { DataTable } from '../../src/dataTable/dataTable';
import { ContentRow } from '../../src/dataTable/row/contentRow';
import { HeaderRow } from '../../src/dataTable/row/headerRow';

// tslint:disable-next-line:no-var-requires
require('jsdom-global')();
const should = chai.should();

describe('DataTable', () => {
  const testFilePath = '/path/to/dir/testFileName.fm';
  let dataTable = new DataTable(testFilePath);

  beforeEach(() => {
    dataTable = new DataTable(testFilePath);
  });

  it('should exist', () => {
    dataTable.should.exist;
  });

  it('should have a filePath string', () => {
    dataTable.should.have.property('filePath')
    .that.is.a('string');
  });

  it ('should not require filePath', () => {
    dataTable = new DataTable();

    should.equal(dataTable.filePath, undefined);
  });

  it('should store a filePath when instantiated', () => {
    dataTable.should.have.property('filePath')
    .that.equals(testFilePath);
  });

  it ('should have a rows array property', () => {
    dataTable.should.have.property('rows')
    .that.is.instanceOf(Array);
  });

  it ('should be able to add rows', () => {
    const row = new ContentRow(dataTable);

    dataTable.addRow();
    dataTable.addRow(row);
  });

  it ('should be able to add multiplt rows at once', () => {
    const rows = [new ContentRow(dataTable),
                  new ContentRow(dataTable),
                  new ContentRow(dataTable)];
    dataTable.addRows(rows);

    dataTable.rows.should.deep.equal(rows);
  });

  it ('should pad rows to header length', () => {
    const headerArray = ['', '', ''];
    dataTable.headerRow =  HeaderRow.fromArray(dataTable, headerArray);

    dataTable.addRow();

    dataTable.rows[0].numCells.should.equal(headerArray.length);
  });

  it ('should pad header to new row length', () => {
    const rowArray = ['', '', ''];
    dataTable.addRow(ContentRow.fromArray(dataTable, rowArray));

    dataTable.rows[0].numCells.should.equal(rowArray.length);
  });

  it ('should be able to add columns', () => {
    dataTable.addRow();

    const numCols = 10;
    for (let i = 0 ; i < numCols ; i++) {
      dataTable.addColumn();
    }

    dataTable.headerRow.numCells.should.equal(numCols);
  });

  it ('should maintain added rows in order', () => {
    const rows = [new ContentRow(dataTable, [new Cell(1)]),
                  new ContentRow(dataTable, [new Cell(2)]),
                  new ContentRow(dataTable, [new Cell(3)])];
    dataTable.addRows(rows);

    dataTable.rows.should.deep.equal(rows);
  });

  it ('should be able to instantiate from an array without header', () => {
    const table = [
      ['a1', 'b1', 'c1'],
      ['a2', 'b2', 'c2'],
      ['a3', 'b3', 'c3'],
    ];

    const fromArray = DataTable.fromStringArray({
      hasHeader: false,
      rowArray: table,
    });

    fromArray.headerRow.cells.should.have.lengthOf(table[0].length);
    fromArray.headerRow.cells.forEach((c, i) => {
      c.contents.should.equal('');
    });

    fromArray.rows.forEach((r, i) => {
      r.cells.forEach((c, j) => {
        c.contents.should.equal(table[i][j]);
      });
    });
  });

  it ('should be able to instantiate from an array with a header', () => {
    const table = [
      ['a1', 'b1', 'c1'],
      ['a2', 'b2', 'c2'],
      ['a3', 'b3', 'c3'],
    ];

    const fromArray = DataTable.fromStringArray({
      hasHeader: true,
      rowArray: table,
    });

    fromArray.headerRow.cells.forEach((c, i) => {
      c.contents.should.equal(table[0][i]);
    });

    fromArray.rows.forEach((r, i) => {
      r.cells.forEach((c, j) => {
        c.contents.should.equal(table[i + 1][j]);
      });
    });
  });

  it ('should be able to instantiate from a csv with a header row', () => {
    const table = `
    "h1","h2","h3"
    "a1","b1","c3"
    "a2","b2","c3"
    "a3","b3","c3"
    `;
    const splitTable = table.split('\n').map( (r) => r.split(','));

    const fromCsv = DataTable.fromCsv({
      csvString: table,
      hasHeader: true,
    });

    fromCsv.headerRow.cells.forEach((c, i) => {
      c.contents.should.equal(splitTable[0][i]);
    });

    fromCsv.rows.forEach((r, i) => {
      r.cells.forEach((c, j) => {
        c.contents.should.equal(splitTable[i + 1][j]);
      });
    });
  });

  it ('should be able to instantiate from a csv without a header row', () => {
    const table = '"a1","b1","c3"\n"a2","b2","c3"\n"a3","b3","c3"';
    const splitTable = table.replace(/"/g, '').split('\n').map( (r) => r.split(','));

    const fromCsv = DataTable.fromCsv({
      csvString: table,
      hasHeader: false,
    });

    fromCsv.headerRow.cells.should.have.lengthOf(splitTable[0].length);
    fromCsv.headerRow.cells.forEach((c, i) => {
      c.contents.should.equal('');
    });

    fromCsv.rows.forEach((r, i) => {
      r.cells.forEach((c, j) => {
        c.contents.should.equal(splitTable[i][j]);
      });
    });
  });


  it ('should handle an empty string array instantiation', () => {
    const table: string[][] = [];

    const fromArray = DataTable.fromStringArray({
      hasHeader: false,
      rowArray: table,
    });

    fromArray.headerRow.cells.should.have.lengthOf(0);
    fromArray.headerRow.cells.forEach((c, i) => {
      c.contents.should.equal('');
    });

    fromArray.rows.forEach((r, i) => {
      r.cells.forEach((c, j) => {
        c.contents.should.equal(table[i][j]);
      });
    });
  });

  it ('should be able to generate a dom element', () => {
    const table = dataTable.toDomElement();

    table.should.be.an.instanceof(HTMLTableElement);
  });

  it ('should persist its dom element', () => {
    // tslint:disable-next-line:no-string-literal
    should.equal(dataTable['domElement'], undefined);
    const table = dataTable.toDomElement();

    // tslint:disable-next-line:no-string-literal
    table.should.equal(dataTable['domElement']);
  });

  it ('should update its dom element', () => {
    const initTable = dataTable.toDomElement();
    dataTable.addColumn();

    // tslint:disable-next-line:no-string-literal
    initTable.should.equal(dataTable['domElement']);
    initTable.firstChild.firstChild.childNodes.length.should.equal(2);
  });

  it ('should maintain dom element row number when increasing column count', () => {
    const rows = [new ContentRow(dataTable), new ContentRow(dataTable), new ContentRow(dataTable)];
    dataTable.addRows(rows);
    const element = dataTable.toDomElement();

    dataTable.addColumn();

    const rowElements = element.children[1].children;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0 ; i < rowElements.length ; i++) {
      rowElements[i].childNodes.length.should.equal(1);
    }
  });

  it ('should maintain dom element column number when increasing row count', () => {
    const element = dataTable.toDomElement();

    [1, 2, 3].forEach((i) => {
      dataTable.addColumn();
      const rowElements = element.children[1].children;
      // tslint:disable-next-line:prefer-for-of
      for (let j = 0 ; j < rowElements.length ; j++) {
        rowElements[j].childNodes.length.should.equal(i);
      }
    });
  });
});
