# FlashMatrix
[![pipeline status](https://gitlab.com/mdgibson/FlashMatrix/badges/master/pipeline.svg)](https://gitlab.com/mdgibson/FlashMatrix/commits/master)
[![coverage report](https://gitlab.com/mdgibson/FlashMatrix/badges/master/coverage.svg)](https://mdgibson.gitlab.io/FlashMatrix/coverage)

A matrix-based study aid. FlashMatrix allows you to input spreadsheets and randomly blacks out cells to help form strong recall of an array of information.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To install development dependencies for developing, testing, and building FlashMatrix, use [`npm`](https://docs.npmjs.com/).

For Linux, simply install the npm package using your preferred package manager. For example, Arch-based distros would use:

```
pacman -S npm
```

For Mac OS:

```
brew install node
```

For Windows, npm is included with the [`Node.js`](https://nodejs.org/en/download/) installer.

### Installing

Use npm to install all required dependencies

```
npm install
```

Wait for all packages to be installed. Afterward, you can start FlashMatrix

```
npm run start
```

## Running the tests

Npm scripts have been written to automate test execution. Two scripts are provided. 

```
npm run test
```

```
npm run coverage
```

Runs all tests. The latter also generates a test coverage report output to the `coverage` directory.

```
npm run test:watch
```

Runs the [`Mocha`](https://mochajs.org/) testing framework in `watch` mode. It will rerun all tests upon a change to a package file.

## Deployment

```
npm run dist
```

Produces a distribution archive for your system.

```
npm run dist:system
```

will build the package for `system` (valid options: linux, win, mac). These options may (probably will) require additional cross-compilation dependencies.

## Authors

* **Matt Gibson** - *Initial work*

See also the list of [contributors](https://gitlab.com/mdgibson/FlashMatrix/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Michelle Goodwin for product request and testing

